import argparse
from main_package.a_class import AClass
from main_package.ops import operation_one, operation_two

parser = argparse.ArgumentParser()
parser.add_argument("action", type=str)
parser.add_argument("other_args", nargs="+", type=int)


def main(parser):
    actions = {"class": AClass.run,
               "op1":operation_one,
               "op2":operation_two}

    arguments = parser.parse_args()

    print(actions[arguments.action](*arguments.other_args))

if __name__ == '__main__':
    main(parser)