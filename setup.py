from setuptools import setup, find_packages

setup(
    name='ci_cd_demo',
    packages=find_packages(),
    description='CI_CD demo for bitbucket pipelines',
    use_scm_version=True,
    scripts=["ci_cd_main.py"],
    author='Cristiano Werner Araujo',
    author_email="cristiano.werner@bairesdev.com",
    url='',
    classifiers=[],
    setup_requires=["setuptools_scm"],
    install_requires=["termcolor"]
)
