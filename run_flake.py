from subprocess import getoutput

flake8_output = getoutput("flake8 . --extend-exclude venv --count")

print(flake8_output)

count = int(flake8_output.split("\n")[-1])

FLAKE8_WARNING_COUNT = 10

exit(count > FLAKE8_WARNING_COUNT)
