import unittest
from main_package import a_class

class TestAClass(unittest.TestCase):
    def test_run(self):
        object_under_test = a_class.AClass.run(1, 2)
        self.assertIsInstance(object_under_test, a_class.AClass)

    def test_sum(self):
        object_under_test = a_class.AClass(1,2)
        self.assertIsNone(object_under_test._sum)
        self.assertEqual(object_under_test.sum, 3)
        self.assertIsNotNone(object_under_test._sum)

    def test_append(self):
        object_under_test = a_class.AClass(1, 2)
        object_under_test.append(1)
        object_under_test.append(2)
        object_under_test.append(3)
        self.assertEqual(len(object_under_test), 3)

    def test_raise_exception_append_not_int(self):
        object_under_test = a_class.AClass(1, 2)
        with self.assertRaises(AssertionError):
            object_under_test.append("1")

if __name__ == '__main__':
    unittest.main()
