import unittest
from unittest.mock import patch
import main_package.ops


class TestOps(unittest.TestCase):
    @patch("main_package.ops.print")
    def test_op_1(self, mock):
        main_package.ops.operation_one()
        self.assertEqual(mock.call_count, 1)

    @patch("main_package.ops.cprint")
    def test_op_2(self, mock):
        main_package.ops.operation_two()
        self.assertEqual(mock.call_count, 1)


if __name__ == '__main__':
    unittest.main()
