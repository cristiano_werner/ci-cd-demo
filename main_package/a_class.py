from termcolor import colored

class AClass:
    def __init__(self, param1, param2):
        self.param1 = param1
        self.param2 = param2
        self._sum = None
        self.list = []


    @property
    def sum(self):
        if not self._sum:
            self._sum = self.param1 + self.param2
        return self._sum

    def append(self, item):
        assert isinstance(item, int)
        self.list.append(item)

    def remote(self, value):
        self.list.remove(value)

    def __len__(self):
        return len(self.list)

    def __str__(self):
        return colored(f"{self.param1} - {self.param2}", "yellow")

    @classmethod
    def run(cls, p1, p2):
        instance = cls(p1, p2)
        return instance
